# как отключить легенду

За управление отображением легенд, отвечает параметр для опций `showLegend`
Например:
```javascript
chart = nv.models.lineChart()
    .options({
        duration: 300,
        useInteractiveGuideline: false,
        showLegend: false
    });
```

или так:

```javascript
chart = nv.models.lineChart()
    .showLegend(true)
    .options({
        duration: 300,
        useInteractiveGuideline: false
    });
```


Решение применимо как минимум для типа `chartLine`

Для динамического включения или отключения легенды (аналогично для любой опции):
```javascript
chart.options({showLegend: true});
chart.update();
```
или
```javascript
chart.showLegend(true);
chart.update();
```
