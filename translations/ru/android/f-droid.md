# Background

## Что такое f-droid

F-Droid — это каталог свободных приложений с открытым исходным кодом
(FOSS) для платформы Android. Программа позволяет легко находить и
устанавливать приложения на ваше устройство и отслеживать их
обновления.

* <https://f-droid.org/ru/>

## Установка

### Вариант 1: Установка через QR-код

* [Открыть официальный сайт](https://f-droid.org/)
* С помощью телефона считать QR-код и выполнить загруженное
      приложение `Fdroid.apk`

### Вариант 2: Прямая установка через скачивание

  * [Скачать `Fdroid.apk`](https://f-droid.org/FDroid.apk)
    * Опционально (шаг можно пропустить): сохранить на отдельном носителе для последуюшей
      загрузки на телефон через USB
  * Выполнить, открыв загруженный файл `Fdroid.apk`

## Дополнительно

### Проверка целостности файла `Fdroid.apk`

Быстрая проверка gpg-подписи:

```
gpg --verify FDroid.apk.asc FDroid.apk
gpg: Signature made Thu Apr 11 14:41:19 2019 EET
gpg:                using RSA key 7A029E54DD5DCE7A
gpg: Good signature from "F-Droid <admin@f-droid.org>" [unknown]
gpg: Note: This key has expired!
Primary key fingerprint: 37D2 C987 89D8 3119 4839  4E3E 41E7 044E 1DBA 2E89
     Subkey fingerprint: 802A 9799 0161 1234 6E1F  EFF4 7A02 9E54 DD5D CE7
```

Верификация sha256-хэша:

```
sha256sum FDroid.apk
dc94be308d453241547b442af87c2b9bbadc3f4c4df68228313d9c0f544ffcc8  FDroid.apk
```

Инспектирование в публичных источниках:

- https://duckduckgo.com/?q=%22dc94be308d453241547b442af87c2b9bbadc3f4c4df68228313d9c0f544ffcc8%22&t=hz&ia=web>
- https://www.apkscan.org/app/006B54C1282729C673855AD0EDB8E3B7D7A65782>
