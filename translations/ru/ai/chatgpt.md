# Общие рекомендации
Сервисы ChatGPT могут быть недоступны.

Для облегчения доступа рекомендуется использовать VPN, используя
рекомендованные расширения и сервисы, приведённые ниже.

В браузере Firefox используется технология контейнеров, которая
позволяет изолировать и сделать независимым доступ в отдельных
вкладках.

Фактически, эта технология позволяет входить в разных вкладках под разными учетками: [более подробно](https://searx.org/search?q=%D0%B4%D0%BB%D1%8F%20%D1%87%D0%B5%D0%B3%D0%BE%20%D0%BD%D1%83%D0%B6%D0%BD%D1%8B%20%D0%BA%D0%BE%D0%BD%D1%82%D0%B5%D0%B9%D0%BD%D0%B5%D1%80%D1%8B%20%D0%B2%D0%BA%D0%BB%D0%B0%D0%B4%D0%BE%D0%BA%20firefox&language=auto&time_range=&safesearch=0&categories=general)

Если не используется расширение для контейнеров (это расширение работает
только для Firefox), то, возможен вход только под одной учеткой. Во
всяком случае это не тестировалось.

Чтобы пользоваться контейнерами, необходимо установить расширение, затем
создать отдельный контейнер и открыть в нём ChatGPT сервис.

Расширения для переключения прокси тоже могут быть полезны, когда
используется VPN-доступ выборочно для отдельных сайтов и для
*.openai.com в частности.

Установка выборочного доступа требует некоторых дополнительных действий
административного уровня и в случае каких либо проблем можно
устанавливать VPN-доступ глобально. В этом случае, такие расширения как FoxyProxy не нужны.

Для безопасного приватного доступа рекомендуется использовать:
  * [браузер Firefox](https://www.mozilla.org/ru/firefox/browsers/)
  * VPN
    * расширения для Firefox
      * переключатели для прокси
        * https://addons.mozilla.org/firefox/addon/foxyproxy-standard/
          * [исходный код и страница
            проекта](https://github.com/foxyproxy/browser-extension)
    * расширения для Chromium-совместимых браузеров
      * переключатели для прокси
        * https://chrome.google.com/webstore/detail/foxyproxy-standard/gcknhkkoolaabfmlnjonogaaifnjlfnp?hl=en
          * [исходный код](https://addons.mozilla.org/firefox/addon/foxyproxy-standard/)
  * контейнеры (изолирует доступ в отдельной влкадке)
    * https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
# Полезные ссылки
## Основной сервис ChatGPT
  * https://chat.openai.com/auth/login
## ChatGPT без ограничений
  * https://perplexity.ai
    * данный сервис можно использовать без каких-либо ограничений
    * рекомендуется создать учетку, но можно без регистрации
## Коллекция awesome-ресурсов
  * https://github.com/ai-collection/ai-collection/blob/main/README.ru.md#%D1%87%D0%B0%D1%82-%D0%B1%D0%BE%D1%82
    * ссылка на агрегаторе awesome-ресурсов
      * https://www.trackawesomelist.com/ai-collection/ai-collection/readme/#chat-bot
