```
Не бойся, что не знаешь – бойся, что не учишься.

```
# Технологии, способные нагнать на Вас страх:

 *  json
 *  yaml
 *  xml
 *  xslt
 *  html
 *  css
 *  http
 *  https
 *  csv
 *  curl
 *  ajax
 *  javascript
 *  регулярные выражения
 *  симметричное и асимметричное шифрование
 *  svg
 *  git
 *  rdp
 *  vnc
 *  [хэширование](https://web.archive.org/web/20140228081623/http://www.ssl.stu.neva.ru/psw/crypto/appl_rus/appl_cryp.htm#top)
 *  linux
 *  облегченные языки разметки
 *  emacs
 *  rest
 *  ssh
 *  SOLID, YAGNI, DRY
 *  паттерны проектирования
 *  MIME
 *  base64
 *  smtp
 *  [и т.д. ](./skills.md)


## Примечание
   Если Вы, всё-же однозначно напуганы и/или заблудились, рекомендуется
   безотлагательно и внимательно ознакомиться с данными материалами:

   - [XY-проблема](https://habr.com/ru/company/dodopizzadev/blog/467047/)
   - [Как правильно задавать вопросы](http://segfault.kiev.ua/smart-questions-ru.html)
   - [бритва Хэнлона](https://dic.academic.ru/dic.nsf/ruwiki/425319)

## Индекс связанных документов
  * [Технологии, способные нагнать на Вас страх](./scope.md#%D1%82%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8-%D1%81%D0%BF%D0%BE%D1%81%D0%BE%D0%B1%D0%BD%D1%8B%D0%B5-%D0%BD%D0%B0%D0%B3%D0%BD%D0%B0%D1%82%D1%8C-%D0%BD%D0%B0-%D0%B2%D0%B0%D1%81-%D1%81%D1%82%D1%80%D0%B0%D1%85)
  * [веб-разработка: расширенный список навыков](./skills.md)
  * [поиск информации: рекомендации](./search.md)
