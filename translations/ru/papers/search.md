```
Заблудиться в трёх соснах гораздо проще чем кажется, особенно когда
сомневаешься или не знаешь.
```
# Введение

  Знать все и много практически не реально.

  Поэтому, разумно знать только то, что необходимо, важно и полезно в
  первую очередь. Для всего остального остается поиск и фокус на том,
  что Вы хотите в актуальный/данный момент.

  Вышесказанное, означает, что все, что касается поиска и соответствущих
  навыков очень важно. Даже более чем. Плюс, просто необходимость быть
  своего рода психоаналитиком по отношению как минимум к себе. Ведь это
  довольно странно, спрашивать других что мы хотим. В лучшем случае
  другие могут только помочь в этом, но не решить за Вас или взять на
  себя ответственность что Вы хотите.

# Как искать

Правильно сформулированный вопрос - уже половина ответа. Примерный
алгоритм поиска:

  * на начальных этапах полезно еще раз осознать, понять что Вам нужно, что Вы хотите
  * если это пока сложно, начинайте формулировать с этим ключевые
  слова, описывающее запрос, ассоциации
  * продолжайте формировать запрос, употребляя самые точные и как можно
  более детальные понятия, ключевые слова, специфичные термины
  * помните - чем точнее и детальнее будет запрос, тем более точным
  будет ответ. Чем более оригинальная и **характерная** комбинация
  ключевых слов, хорошо описыващая запрос, тем
  меньше будет результатов и спама в выдаче, что само по себе хорошо
  т.к. упрощает анализ и восприятие. В конечном итоге это может
  существенно повысить точность ответов.
  * используйте разные поисковые машины для полноты исследования


## Полезное
  - запросы
    - [Инфографика: специальные приемы для онлайн исследований при
  поиске (несколько устарело)](https://lifehacker.com/the-get-more-out-of-google-infographic-summarizes-onlin-5864111)
      - [42 оператора расширенного поиска Google (полный
        список)](https://habr.com/ru/post/437618/)
  - полезные расширения для браузера
    - Firefox
      - https://addons.mozilla.org/en-US/firefox/addon/duckduckgoogle/
      - https://addons.mozilla.org/en-US/firefox/addon/to-google-translate/
      - https://addons.mozilla.org/en-US/firefox/addon/languageswitch/

## Индекс связанных документов
  * [Технологии, способные нагнать на Вас страх](./scope.md#%D1%82%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8-%D1%81%D0%BF%D0%BE%D1%81%D0%BE%D0%B1%D0%BD%D1%8B%D0%B5-%D0%BD%D0%B0%D0%B3%D0%BD%D0%B0%D1%82%D1%8C-%D0%BD%D0%B0-%D0%B2%D0%B0%D1%81-%D1%81%D1%82%D1%80%D0%B0%D1%85)
  * [веб-разработка: расширенный список навыков](./skills.md)
  * [поиск информации: рекомендации](./search.md)
